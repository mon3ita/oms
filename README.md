# OMS

![go.png](https://i.postimg.cc/W4CCNXJB/go.png)

Inspired by `dir` from `python` and `methods` from `ruby`.

Returns all/public/private methods of an object.

### Install

`go get bitbucket.org/mon3ita/oms`

### Example

```
package main

import (
    "bitbucket.org/mon3ita/oms"

    "fmt"
)

func main() {
    var dummyVar *os.File

    pkgs := oms.PublicMethods(dummyVar)

    fmt.Println(pkgs)

    /*
    Result:
    map[c:\go\src\os:[Readdir Readdirnames Name 
        Read ReadAt Write WriteAt Seek WriteString Chmod SetDeadline SetReadDeadline SetWriteDeadline SyscallConn Fd Close Stat Truncate Sync Chown 
        Chdir Chown Truncate Sync Chdir Fd Close Fd Close Stat Stat]]
    */
}

```

### To Do

Currently it doesn't work with primitive types, but only with all packages in your GOROOT, or GOPATH.

### Updates

08/01/2021 Now map keys are all paths in which the type of a variable is used.