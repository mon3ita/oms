package oms

import (
	//"reflect"

	//"runtime"
	//"os/exec"
	"path/filepath"

	//"strings"
	//"regexp"
	//"errors"

	//"fmt"

	"go/build"
	"os"

	"fmt"
)

var (
	goroot = "GOROOT"
	gopath = "GOPATH"
)

func getPath(gotype string) string {
	path := os.Getenv(gotype)

	if path == "" && gotype == gopath {
		path = build.Default.GOPATH
	} else if path == "" {
		path = build.Default.GOROOT
	}

	path = filepath.Join(path, "src")
	return path
}

func findGoPaths() []string {

	paths := []string{
		getPath(goroot),
		getPath(gopath),
	}

	fmt.Println(paths)

	return paths
}
