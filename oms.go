package oms

import (
	"reflect"

	"github.com/thoas/go-funk"
	"unicode"
)

func PublicMethods(o interface{}) map[string][]string {

	paths := findGoPaths()
	pkgs := getGoPkgs(reflect.TypeOf(o).String(), paths)

	for key, _ := range pkgs {
		pkgs[key] = funk.FilterString(pkgs[key], func(name string) bool {
			return unicode.IsUpper(rune(name[0]))
		})
	}

	return pkgs
}

func PrivateMethods(o interface{}) map[string][]string {

	paths := findGoPaths()
	pkgs := getGoPkgs(reflect.TypeOf(o).String(), paths)

	for key, _ := range pkgs {
		pkgs[key] = funk.FilterString(pkgs[key], func(name string) bool {
			return unicode.IsLower(rune(name[0]))
		})
	}

	return pkgs
}

func AllMethods(o interface{}) map[string][]string {
	paths := findGoPaths()
	pkgs := getGoPkgs(reflect.TypeOf(o).String(), paths)

	return pkgs
}
