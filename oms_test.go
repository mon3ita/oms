package oms

import (
	"testing"

	"os"
)

func TestPublicMethods(t *testing.T) {
	var dummyVar *os.File

	pkgs := PublicMethods(dummyVar)

	if len(pkgs) == 0 {
		t.Errorf("At least one package must exist, but none were found")
	}
}

func TestPrivateMethods(t *testing.T) {
	var dummyVar *os.File

	pkgs := PrivateMethods(dummyVar)

	if len(pkgs) == 0 {
		t.Errorf("At least one package must exist, but none were found")
	}
}
