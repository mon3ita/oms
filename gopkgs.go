package oms

import (
	"math"
	"path/filepath"

	"bufio"
	"log"
	"os"
	"regexp"
	"strings"
	"sync"
)

var (
	gopkgs map[string][]string
	m      sync.Mutex
)

func readFiles(pkg string, files []string, path string, wg *sync.WaitGroup) {

	pkgParts := strings.Split(pkg, ".")
	structName := pkgParts[1]

	var re_pkg *regexp.Regexp

	if strings.HasSuffix(path, pkgParts[0][1:]) {
		re_pkg = regexp.MustCompile(`func\s*\((\w+)*\s*\*\s*` + structName + `\)\s*\w+\(`)
	} else {
		re_pkg = regexp.MustCompile(`func\s*\((\w+)*\s*\*\s*` + pkg[1:] + `\)\s*\w+\(`)
	}

	re_pkg_method := regexp.MustCompile(`\s*\w+\(`)
	for _, file := range files {
		if strings.HasSuffix(file, "_test.go") {
			continue
		}

		fullpath := filepath.Join(path, file)
		content, err := os.Open(fullpath)

		if err != nil {
			log.Fatal(err)
		}

		defer content.Close()

		scanner := bufio.NewScanner(content)
		for scanner.Scan() {
			bytes := scanner.Bytes()

			matches := re_pkg.FindAllIndex(bytes, 1)

			if len(matches) > 0 {
				for _, match := range matches {

					m.Lock()

					method := re_pkg_method.FindIndex(bytes[match[0]:match[1]])

					methodNameBegin := method[0] + 1
					methodNameEnd := method[1] - 1

					gopkgs[path] = append(gopkgs[path], string(bytes[methodNameBegin:methodNameEnd]))
					m.Unlock()
				}
			}
		}
	}

	wg.Done()
}

func readDirs(pkg string, paths []string, wg *sync.WaitGroup) {

	var wgFiles sync.WaitGroup

	for _, path := range paths {

		file, err := os.Open(path)

		if err != nil {
			log.Fatal(err)
		}

		files, err := file.Readdirnames(0)

		var oFiles []string
		var dirFiles []string

		for _, file := range files {
			fullpath := filepath.Join(path, file)

			info, err := os.Stat(fullpath)

			if err != nil {
				log.Fatal(err)
			}

			if info.IsDir() {
				dirFiles = append(dirFiles, fullpath)
			} else {
				oFiles = append(oFiles, fullpath)
			}
		}

		if len(dirFiles) > 0 {
			wg.Add(1)
			go readDirs(pkg, dirFiles, wg)
		}

		if len(files) > 0 {
			ncor_ := math.Log2(float64(len(files)))
			ncor := int(ncor_)

			if ncor == 0 {
				ncor = 1
			}

			split := len(files) / ncor
			j := 0
			for i := 0; i < ncor; i++ {
				wgFiles.Add(1)
				go readFiles(pkg, files[j:split], path, &wgFiles)

				j = split
				split += split

				if split > len(files) {
					split = len(files)
				}
			}
		}
	}

	wgFiles.Wait()
	wg.Done()
}

func getGoPkgs(pkg string, paths []string) map[string][]string {

	gopkgs = make(map[string][]string)

	var wg sync.WaitGroup

	ncor_ := math.Log2(float64(len(paths)))
	ncor := int(ncor_)

	if ncor == 0 {
		ncor = 1
	}

	split := len(paths) / ncor
	j := 0
	for i := 0; i < ncor; i++ {
		wg.Add(1)
		go readDirs(pkg, paths[j:split], &wg)

		j = split
		split += split

		if split > len(paths) {
			split = len(paths)
		}
	}

	wg.Wait()

	return gopkgs
}
